import { ref } from 'vue'
import { defineStore } from 'pinia'
interface AddressBook{
      id: number
      name:string
      tell: string
      gender: string
  
  }
export const useAddressBookStore = defineStore('address_book', () => {


  
  let lastId =1
  const address = ref<AddressBook>({
      id: 0,
      name: '',
      tell:'',
      gender:'Male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddNew = ref(false)
  function Save(){
      if(address.value.id>0){
          //edit
          const editedIndex = addressList.value.findIndex((item)=> item.id ===address.value.id)
          addressList.value[editedIndex]=address.value
      } else{
          //add new
          addressList.value.push({...address.value,id: lastId++})
      }
      isAddNew.value=false
      address.value={
          id: 0,
          name: '',
          tell:'',
          gender:'Male'
      }
  }
  function edit(id: number)   {
      isAddNew.value=true
      const editIndex = addressList.value.findIndex((item)=>item.id===id)
      // copy object
      address.value = JSON.parse(JSON.stringify(addressList.value[editIndex]))
  }
  function remove(id: number)   {
      const removedIndex = addressList.value.findIndex((item)=>item.id===id)
      addressList.value.splice(removedIndex, 1)
      
  }
  function cancle(){
      isAddNew.value=false
      address.value={
          id: 0,
          name: '',
          tell:'',
          gender:'Male'
      }
  }

  return { address,addressList,Save,isAddNew,edit,remove,cancle}
})
